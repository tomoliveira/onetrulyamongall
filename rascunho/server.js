/**
Before running:
> npm install ws
Then:
> node server.js
> open http://localhost:8080 in the browser
*/

const http = require('http');
const fs = require('fs');
const ws = new require('ws');
const wss = new ws.Server({noServer: true});
const clients = new Set();
let howMuch = 0;
  
function accept(req, res) {
  if (req.url == '/ws' && req.headers.upgrade &&
      req.headers.upgrade.toLowerCase() == 'websocket' &&
      req.headers.connection.match(/\bupgrade\b/i)) {
    wss.handleUpgrade(req, req.socket, Buffer.alloc(0), onSocketConnect);
  }else if(req.url == '/') { // index.html
    fs.createReadStream('./index.html').pipe(res);
  }else{
    res.writeHead(404);
    res.end();
  }
}
function onSocketConnect(ws) {
  clients.add(ws);
  console.log('con');
  howMuch++;
  ws.on('message', function(message) {
    //console.log(`message received: ${message}`);
    if(message){
      let arr = [];
      for(let client of clients){
        if(client !== ws){
          client.send(message);
        }
      }
    }
  });
  ws.on('close', function() {
    log(`connection closed`);
    howMuch--;
    clients.delete(ws);
  });
}
let log;
if (!module.parent) {
  log = console.log;
  http.createServer(accept).listen(4000);
} else {
  log = function() {};
  exports.accept = accept;
}