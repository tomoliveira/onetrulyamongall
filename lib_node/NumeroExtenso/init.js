const numeroExtenso = require("./extenso.js");
const readline = require('readline');
const rl = readline.createInterface({input: process.stdin,output: process.stdout});
rl.question("Numero: ", function(answer){
    let num = numeroExtenso.NumeroExtenso(answer);
    console.log(num);
    rl.close();
});
