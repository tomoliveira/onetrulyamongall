
module.exports = {
    NumeroExtenso : function(value){let Num= parseFloat(value);if(value == 0){return "zero";}else{
        let integer = parseInt(value);
        if(integer < 1000000000000000){
            let res = Num.toFixed(2) - integer.toFixed(2); res=res.toFixed(2);
            let valor =  integer.toString(), cont=valor.length,ext="", num1, num2, num3,
            u = ["", "um", "dois", "três", "quatro", "cinco","seis", "sete", "oito", "nove", "dez", "onze","doze", "treze", "quatorze", "quinze", "dezesseis","dezessete", "dezoito", "dezenove"],
            c = ["", "cento", "duzentos", "trezentos","quatrocentos", "quinhentos", "seiscentos","setecentos", "oitocentos", "novecentos"],
            d =  ["", "", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa"],
            singular = ["", "mil", "milhão", "bilhão", "trilhão"],plural = ["", "mil", "milhões", "bilhões", "trilhões"];
            for (let i=cont;i > 0;i--){   
                let verifica1="",verifica2=0,verifica3=0,num2="",num3="",num1=0;num2 = valor.substr(cont-i,1);num1 = parseInt(num2);
                if((i==14)||(i==11)||(i==8)||(i==5)||(i==2)){num2 = valor.substr(cont-i,2);num1 = parseInt(num2);}
                if((i==15)||(i==12)||(i==9)||(i==6)||(i==3)){ext=ext+c[num1];num2 = valor.substr(cont-i+1,1);num3 = valor.substr(cont-i+2,1);
                if((num2!="0")||(num3!="0")){ext+=" e ";}
                }else if(num1>19){num2 = valor.substr(cont-i,1);num1 = parseInt(num2);ext=ext+d[num1];num3 = valor.substr(cont-i+1,1);if((num3!="0")&&(num2!="1")){ext+=" e ";}
                }else if((num1<=19)&&(num1>9)&&((i==14)||(i==11)||(i==8)||(i==5)||(i==2))){ext=ext+u[num1];   
                }else if((num1<10)&&((i==13)||(i==10)||(i==7)||(i==4)||(i==1))){num3 = valor.substr(cont-i-1,1);if((num3!="1")&&(num3!="")){ext=ext+u[num1];}}
                if(i%3==1){       
                    verifica3 = cont-i;
                    if(verifica3==0){verifica1 = valor.substr(cont-i,1);}
                    if(verifica3==1){verifica1 = valor.substr(cont-i-1,2);}
                    if(verifica3>1){verifica1 = valor.substr(cont-i-2,3);}
                verifica2 = parseInt(verifica1);
                if(i==13){
                    if(verifica2==1){ext=ext+" "+singular[4]+" ";}
                    else if(verifica2!=0){ext=ext+" "+plural[4]+" ";}}
                    if(i==10){if(verifica2==1){ext=ext+" "+singular[3]+" ";}
                    else if(verifica2!=0){ext=ext+" "+plural[3]+" ";}}
                    if(i==7){if(verifica2==1){ext=ext+" "+singular[2]+" ";}
                    else if(verifica2!=0){ext=ext+" "+plural[2]+" ";}}
                    if(i==4){if(verifica2==1){ext=ext+" "+singular[1]+" ";}
                    else if(verifica2!=0){ext=ext+" "+plural[1]+" ";}}
                    if(i==1){if(verifica2==1){ext=ext+" ";}else{ext=ext+" ";}}
                }
            }
            res = res * 100;
            let aexCent=0;
            if(res<=19&&res>0){ext+=" e "+u[res];}
            if(res>19){aexCent=parseInt(res/10);ext+=" e "+d[aexCent];res=res-(aexCent*10);if(res!=0){ext+=" e "+u[res];}else {ext;}}return ext;}
            else{return false;}
        }
    }
}