const { app, BrowserWindow } = require('electron')

function createWindow () {
  // Create the browser window.
  let win = new BrowserWindow({
    width: 1200,
    height: 1200,
    webPreferences: {
      nodeIntegration: false,
      webSecurity: false,
      devTools: false
    }
  })
  win.loadFile('willGO/start.html')
}

app.on('ready', createWindow)