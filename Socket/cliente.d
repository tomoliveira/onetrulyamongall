mixin(import("imports.d"));
void resposta(shared Socket srequest){
	while(true){	
		Socket request = cast() srequest;
		char[1024] response;
		size_t len = request.receive(response);
		auto received = response[0..len];
		writeln(received);
        if(received == "quit"){
		    request.close();
			break;
        }
	}
}
void socket(){
	auto request = new TcpSocket();
	request.connect(new InternetAddress("localhost", 8000));
	while(true){
		string msg;
		write("> ");
		readf("%s\n",msg);
		request.send(msg);
		spawn(&resposta, cast(shared)request );
	}
}
void main(){
	socket();
}
