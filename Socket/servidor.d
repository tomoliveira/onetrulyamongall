mixin(import("imports.d"));
//inicia o servidor
void connServidor(){
	Socket server = new TcpSocket();
	server.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, true);
	server.bind(new InternetAddress(8000));
	server.listen(10);
    connClient(server);
}
//decoda todos request
void enviamsg( shared Socket sclient, shared Address[] objRemote){
    while(true){
        char[1024] buffer;
        auto objremote =  cast(Address[]) objRemote;
        Socket client = cast() sclient;
        int i;
        size_t len = client.receive(buffer);
	    auto received = buffer[0..len];
	    write("> ");
	    writefln("%s", received);
        //envia response para todos conectados        
        for(i=0; i< objremote.length; i++){
            client.sendTo(received, objremote[i]);        
            if(received=="quit" || received == null){
                client.shutdown(SocketShutdown.BOTH);
                client.close();
                break;
            }
        }
    }  
}
//mantem servidor ativo para conexoes
void connClient(Socket sclient){  
    Address[] objRemote;
    while(true){
        Socket client = sclient.accept();
        objRemote ~= client.remoteAddress();
        spawn(&enviamsg, cast(shared)client, cast(shared) objRemote);
        writeln(objRemote);
    }
}
void main(){
    connServidor();
}
